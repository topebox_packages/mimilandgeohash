module gitlab.com/topebox_packages/mimigeohash

go 1.18

require github.com/mmcloughlin/geohash v0.10.0
